// all of these values are guessed via staring at replays. the skill levels should be accurate
3810,King Poring@NPC_SUMMONSLAVE,idle,196,10,10000,0,20000,no,self,onspawn,0,1002,,,,,,
3810,King Poring@SM_BASH,attack,5,6,2500,1000,20000,no,target,always,0,,,,,,,100
3810,King Poring@MG_FIREBALL,attack,17,2,2500,1000,10000,no,target,always,0,,,,,,,101
3810,King Poring@AL_DECAGI,attack,30,3,2500,1000,10000,no,target,always,0,,,,,,,102
3810,King Poring@MC_MAMMONITE,attack,42,5,2500,1000,15000,no,target,always,0,,,,,,,103
3810,King Poring@AC_DOUBLE,attack,46,6,2500,1000,10000,no,target,always,0,,,,,,,104
3810,King Poring@TF_POISON,attack,52,3,2500,1000,15000,no,target,always,0,,,,,,,105
3811,Giant Goldring@NPC_STUNATTACK,attack,179,1,10000,1000,15000,no,target,always,0,,,,,,,
// NPC_WIDECONFUSE2 is NPC_WIDECONFUSE .. but the status lasts 15s instead. apparently? that's stupid!
//3812,Giant Amering@NPC_WIDECONFUSE2,attack,762,1,500,2000,15000,no,self,always,0,,,,,,,
3812,Giant Amering@NPC_WIDECONFUSE,attack,667,1,500,2000,15000,no,self,always,0,,,,,,,
// These seem to just be copies of the normal monster?
3813,Drops@NPC_EMOTION,loot,197,1,2000,0,5000,yes,self,always,0,2,,,,,,
3813,Drops@NPC_FIREATTACK,attack,186,1,2000,0,5000,yes,target,always,0,,,,,,,
3814,Poporing@NPC_EMOTION,loot,197,1,2000,0,5000,yes,self,always,0,2,,,,,,
3814,Poporing@NPC_POISON,attack,176,2,500,800,5000,no,target,always,0,,,,,,,
3814,Poporing@NPC_POISONATTACK,attack,188,1,2000,0,5000,yes,target,always,0,,,,,,,
3815,Poring@NPC_EMOTION,loot,197,1,2000,0,5000,yes,self,always,0,2,,,,,,
3815,Poring@NPC_WATERATTACK,attack,184,1,2000,0,5000,yes,target,always,0,,,,,,,
3816,Marin@MG_FROSTDIVER,attack,15,5,500,1000,5000,yes,target,always,0,,,,,,,
3816,Marin@NPC_EMOTION,loot,197,1,2000,0,5000,yes,self,always,0,2,,,,,,
3816,Marin@NPC_WATERATTACK,attack,184,1,2000,0,5000,yes,target,always,0,,,,,,,
		
// High Orc	Spear Boomerang					
	// Piercing attack	NPC PIERCINGATT				
	// Power up	NPC POWER (low health)				
// Orc Baby	Lick	NPC LICK				
	// SIlence	NPC SILENCEATTACk				
	// Curse	NPC CURSEATTACK				
// Orc lady	Decrease AGI					
	// Piercing attack	NPC PIERCINGATT				
	// Silence					
// Orc Archer	Arrow Shower					
	// Double Strafe					
	// Charge arrow					
	// Piercing attack	NPC PIERCINGATT				
// Orc Warrior	Critical wounds	NPC CRITICALWOUND				
	// Piercing Attack	NPC PIERCINGATT				
	// Defender	NPC DEFENDER				
	// Provoke		

// Hode	Snap					
	// Cloaking					
	// Piercing atk	NPC PIERCINGATT				
	// Fire attack					
// Sandman	Cloaking					
	// Grimtooth					
	// Earth Spike					
	// Defender	NPC DEFENDER				
	// Piercing atk	NPC PIERCINGATT				
// Frilldora	Dark Strike	NPC DARKSTRIKE lv 1				
	// Fireball					



// Willow	Fire wall	500ms cast, skill level 3, recast after ~40s				
	// Fire bolt	1000ms cast, skill level 2, recast after ~20s				
	// Ground Attack	300ms cast, NPC_GROUNDATTACK				
	// Telekinesis	700ms cast NPC_TELEKINESISATTACK, recast after ~40s				
// Stem worm	Pierce	1000ms cast, KN_PIERCE, skill level 3				
	// Silence	500ms cast, NPC_SILENCEATTACK, skill level 2, 10s delay between casts				
	// Piercing atk	500ms cast NPC_PIERCINGATT, 10s delay between casts				
	// Wind Attack	300ms cast NPC_WINDATTACK				
	// Guided Attack	500ms cast NPC_GUIDEDATTACK, skill level2				
	// Blind Attack	200ms cast NPC_BLINDATTACK,				
// Mandragora	Ally heal	700ms cast, 342~355hp heal				
	// Piercing atk	500ms cast NPC_PIERCINGATT				
	// Critical Slash	200ms cast NPC_CRITICALSLASH				
	// Guided Attack	500ms cast, NPC_GUIDEDATTACK				
	// Blood Drain	300ms cast, NPC_BLOODDRAIN on low hp				
	// Pneuma when allow range attacked?
// Spore	Blind Attack	500ms cast, NPC BLINDATTACK				
	// Blood drain	300ms cast, NPC_BLOODDRAIN on low hp		