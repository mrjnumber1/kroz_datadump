<?php
	
// 0 map, 
// 1 globalID, 
// 2 spawn_type 301/300	
// 3 amt <<16 | jobtype
// 4 kname
// 5 sprite
// 6 lv
// 7 (ele*20 + def_ele) << 16 | size << 8 | race

	define('BOSS_TYPE', 301);
	$spawns = [];
	$mobs = [];
	$new_mobs = [];
	
	function ReadMobDb(string $where)
	{
		global $mobs;
		
		$file = file_get_contents($where);
		$lines = explode(PHP_EOL, $file);
		
		foreach ($lines as $line)
		{
			if (strlen($line) == 0 
				|| ($line[0] == "/" && $line[1] == "/"))
				continue;
				
			$mob = str_getcsv($line);
			
			$id = $mob[0];
			$mobs[$id]['id'] = intval($mob[0]);
			$mobs[$id]['sprite'] = strval($mob[1]);
			$mobs[$id]['name'] = strval($mob[2]);
			$mobs[$id]['lv'] = intval($mob[4]);
			$mobs[$id]['ele'] = intval($mob[24]);
			$mobs[$id]['race'] = intval($mob[23]);
			$mobs[$id]['size'] = intval($mob[22]);
			$mobs[$id]['is_boss'] = boolval(intval($mob[25], 0) & 0x2);
		}
	}
	
	ReadMobDb('C:\Users\misterj\Projects\DoM\rathena\db\re\mob_db.txt');
	ReadMobDb('C:\Users\misterj\Projects\DoM\rathena\db\re_import\mob_db.txt');
	
	$file = file_get_contents('zerospawns.txt');
	$lines = explode(PHP_EOL, $file);
	
	$i = 0;
	foreach ($lines as $line)
	{
		if (!strlen($line))
			continue;
		$info = str_getcsv($line);
		
		foreach ($info as &$inf) $inf = trim($inf);
		$map = strval($info[0]);
		$kname = strval($info[4]);
		$sprite = strval($info[5]);
		
		foreach ($info as &$inf) $inf = intval($inf);

		$is_boss = $info[2] == BOSS_TYPE;
		
		$amount = $info[3]>>16;
		$jobtype = $info[3]&0xFFFF;
		$lv = $info[6];
		$race = $info[7] & 0xFF;
		$size = ($info[7]>>8)&0xFF;
		$ele = ($info[7]>>16)&0xFF;
		
		if (!array_key_exists($jobtype, $mobs))
		{
			$mob_info = ['id' => $jobtype, 'sprite' => $sprite, 'name' => $kname,
			'lv' => $lv, 'ele' => $ele, 'race' => $race, 'size' => $size, 'is_boss' => $is_boss];
			$new_mobs[$jobtype] = $mob_info;
			$mobs[$jobtype] = $mob_info;
		}
		
		$spawn = ['mob_id' => $jobtype, 'is_boss' => $is_boss, 'amt'=>$amount];
		$spawns[$map][] = $spawn;
	}


	foreach ($spawns as $map => $spawn)
	{
		foreach ($spawn as $info)
		{
			$mob_id = $info['mob_id'];
			$name = $mobs[$mob_id]['name'];
			$amount = $info['amt'];
			$is_boss = $info['is_boss']?'boss_':'';
			//echo "{$map},0,0,0,0\t{$is_boss}monster\t{$name}\t{$mob_id},{$amount}".PHP_EOL; 
		}
		//echo PHP_EOL;
	}
	
	sort($new_mobs);
	foreach ($new_mobs as $mob_id => $data)
	{
		extract($data);
		$is_boss = (($is_boss)?0:1);
		echo "//{$id},{$sprite},{$name},iROName,{$lv},HP,SP,EXP,JEXP,r1,ATK1,ATK2,DEF,MDEF,STR,AGI,VIT,INT,DEX,LUK,r2,r3,{$size},{$race},{$ele},{$is_boss}".PHP_EOL;
	}