tbl = {

	[19238] = {
		unidentifiedDisplayName = "Ribbon",
		unidentifiedResourceName = "리본",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by ^6A5ACDMagnifier^000000.." },
		identifiedDisplayName = "Poringville Leek",
		identifiedResourceName = "입에무는대파",
		identifiedDescriptionName = {
			"Picked fresh from the Poring Village.",
			"During Physical attacks, chance to transform into a Smokie for 5 seconds, increasing your movement speed by 25% (Does not stack with Agility Up)",
			"Class : ^777777Helm^000000 DEF: ^7777770^000000",
			"Location: ^777777Lower ^000000Weight : ^77777710^000000",
			"Required Level: ^77777730^000000",
			"Usable by: ^777777All jobs^000000"
		},
		slotCount = 0,
		ClassNum = 824,
	},
	[19239] = {
		unidentifiedDisplayName = "Ribbon",
		unidentifiedResourceName = "리본",
		unidentifiedDescriptionName = { "Unknown Item, can be identified by ^6A5ACDMagnifier^000000." },
		identifiedDisplayName = "Poringville Carrot",
		identifiedResourceName = "입에무는홍당무",
		identifiedDescriptionName = {
			"Picked fresh from the Poring Village.",
			"During Physical attacks, chance to transform into a Lunatic for 5 seconds, increasing your movement speed by 25% (Does not stack with Agility Up)",
			"Class : ^777777Helm^000000 DEF: ^7777770^000000",
			"Location: ^777777Lower ^000000Weight : ^77777710^000000",
			"Required Level: ^77777730^000000",
			"Usable by: ^777777All jobs^000000"
		},
		slotCount = 0,
		ClassNum = 829,
	},
	
	[23302] = {
		unidentifiedDisplayName = "Poring Treasure Box",
		unidentifiedResourceName = "상인전직상자",
		unidentifiedDescriptionName = {
			"A box pulled fresh from the Poring Village.",
			"There's no telling what's inside.",
			"Required Level : ^77777730^000000",
			"Weight : ^77777720^000000"
		},
		identifiedDisplayName = "Poring Treasure Box",
		identifiedResourceName = "상인전직상자",
		identifiedDescriptionName = {
			"A box pulled fresh from the Poring Village.",
			"There's no telling what's inside.",
			"Required Level : ^77777730^000000",
			"Weight : ^77777720^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[23649] = {
		unidentifiedDisplayName = "Jello Debris Box",
		unidentifiedResourceName = "포링상자",
		unidentifiedDescriptionName = {
			"Suspicious box with a painting of a Poring",
			"It contains Jelly Fragments.",
			"^ffffff_^000000 ",
			"Weight : ^7777771^000000"
		},
		identifiedDisplayName = "Jello Debris Box",
		identifiedResourceName = "포링상자",
		identifiedDescriptionName = {
			"Suspicious box with a painting of a Poring",
			"It contains Jelly Fragments.",
			"^ffffff_^000000 ",
			"Weight : ^7777771^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	
		[25465] = {
		unidentifiedDisplayName = "Poring Jelly Fragment",
		unidentifiedResourceName = "마나의파편회복",
		unidentifiedDescriptionName = {
			"A fragment found in the Poring Village.",
			"Poring bodies produce this naturally",
			"It can create Physical items when gathered",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		identifiedDisplayName = "Poring Jelly Fragment",
		identifiedResourceName = "마나의파편회복",
		identifiedDescriptionName = {
			"A fragment found in the Poring Village.",
			"Poring bodies produce this naturally",
			"It can create Physical items when gathered.",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[25466] = {
		unidentifiedDisplayName = "Poporing Jelly Fragment",
		unidentifiedResourceName = "마나의파편백은",
		unidentifiedDescriptionName = {
			"A fragment found in the Poring Village.",
			"Poporing bodies produce this naturally",
			"It can create Magical items when gathered.",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		identifiedDisplayName = "Poporing Jelly Fragment",
		identifiedResourceName = "마나의파편백은",
		identifiedDescriptionName = {
			"A fragment found in the Poring Village.",
			"Poporing bodies produce this naturally",
			"It can create Magical items when gathered.",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[25467] = {
		unidentifiedDisplayName = "Drops Jelly Fragment",
		unidentifiedResourceName = "마나의파편서광",
		unidentifiedDescriptionName = {
			"A fragment found in the Poring Village.",
			"Drops bodies produce this naturally",
			"It can create ornamental items when gathered.",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		identifiedDisplayName = "Drops Jelly Fragment",
		identifiedResourceName = "마나의파편서광",
		identifiedDescriptionName = {
			"A fragment found in the Poring Village.",
			"Drops bodies produce this naturally",
			"It can create ornamental items when gathered.",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[25468] = {
		unidentifiedDisplayName = "Deviling Jelly Fragment",
		unidentifiedResourceName = "마나의파편혼돈",
		unidentifiedDescriptionName = {
			"A fragment found in the Poring Village.",
			"Deviling bodies produce this naturally",
			"It can create Speedy items when gathered.",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		identifiedDisplayName = "Deviling Jelly Fragment",
		identifiedResourceName = "마나의파편혼돈",
		identifiedDescriptionName = {
			"A fragment found in the Poring Village.",
			"Deviling bodies produce this naturally",
			"It can create Speedy items when gathered.",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[25469] = {
		unidentifiedDisplayName = "Angeling Jelly Fragment",
		unidentifiedResourceName = "마나의파편황혼",
		unidentifiedDescriptionName = {
			"A fragment found in the Poring Village.",
			"Angeling bodies produce this naturally",
			"It can create Sacred items when gathered.",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		identifiedDisplayName = "Angeling Jelly Fragment",
		identifiedResourceName = "마나의파편황혼",
		identifiedDescriptionName = {
			"A fragment found in the Poring Village.",
			"Angeling bodies produce this naturally",
			"It can create Sacred items when gathered.",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	
	[25470] = {
		unidentifiedDisplayName = "Red Jellystone",
		unidentifiedResourceName = "마법석화",
		unidentifiedDescriptionName = {
			"Poring Jelly Fragment compresses into this high-density Jellystone.",
			"Physical equipment is made from this.",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		identifiedDisplayName = "Red Jellystone",
		identifiedResourceName = "마법석화",
		identifiedDescriptionName = {
			"Poring Jelly Fragment compresses into this high-density Jellystone.",
			"Physical equipment is made from this.",
			"^ffffff_^000000",
			"Weight : ^7777771^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[25471] = {
		unidentifiedDisplayName = "Emerald Jellystone",
		unidentifiedResourceName = "마법석풍",
		unidentifiedDescriptionName = {
			"Poporing Jelly Fragment compresses into this high-density Jellystone.",
			"Magical equipment is made from this.",
			"^ffffff_^000000 ",
			"Weight : ^7777771^000000"
		},
		identifiedDisplayName = "Emerald Jellystone",
		identifiedResourceName = "마법석풍",
		identifiedDescriptionName = {
			"Poporing Jelly Fragment compresses into this high-density Jellystone.",
			"Magical equipment is made from this.",
			"^ffffff_^000000 ",
			"Weight : ^7777771^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[25472] = {
		unidentifiedDisplayName = "Twilight Jellystone",
		unidentifiedResourceName = "마법석지",
		unidentifiedDescriptionName = {
			"Drops Jelly Fragment compresses into this high-density Jellystone.",
			"It is useful for creating ornaments.",
			"^ffffff_^000000 ",
			"Weight : ^7777771^000000"
		},
		identifiedDisplayName = "Twilight Jellystone",
		identifiedResourceName = "마법석지",
		identifiedDescriptionName = {
			"Drops Jelly Fragment compresses into this high-density Jellystone.",
			"It is useful for creating ornaments.",
			"^ffffff_^000000 ",
			"Weight : ^7777771^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[25473] = {
		unidentifiedDisplayName = "Chaotic Jellystone",
		unidentifiedResourceName = "마법석암",
		unidentifiedDescriptionName = {
			"Deviling Jelly Fragment compresses into this high-density Jellystone.",
			"Speedy equipment is made from this.",
			"^ffffff_^000000 ",
			"Weight : ^7777771^000000"
		},
		identifiedDisplayName = "Chaotic Jellystone",
		identifiedResourceName = "마법석암",
		identifiedDescriptionName = {
			"Deviling Jelly Fragment compresses into this high-density Jellystone.",
			"Speedy equipment is made from this.",
			"^ffffff_^000000 ",
			"Weight : ^7777771^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},
	[25474] = {
		unidentifiedDisplayName = "Holy Jellystone",
		unidentifiedResourceName = "마법석성",
		unidentifiedDescriptionName = {
			"Angeling Jelly Fragment compresses into this high-density Jellystone.",
			"Sacred equipment is made from this.",
			"^ffffff_^000000 ",
			"Weight : ^7777771^000000"
		},
		identifiedDisplayName = "Holy Jellystone",
		identifiedResourceName = "마법석성",
		identifiedDescriptionName = {
			"Angeling Jelly Fragment compresses into this high-density Jellystone.",
			"Sacred equipment is made from this.",
			"^ffffff_^000000 ",
			"Weight : ^7777771^000000"
		},
		slotCount = 0,
		ClassNum = 0
	},


}